import { Component, OnInit, ViewChild } from '@angular/core';
import { Code, Decode, Encode } from './models/code.model';
import { CodeService } from './services/code.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  @ViewChild('f') form!: NgForm;

  constructor(private codeService: CodeService) {
  }
  ngOnInit(): void {
  }

  onEncode() {
    const code: Code = {
      password: this.form.value.password,
      message: this.form.value.encode,
    }
    this.codeService.getEncode(code).subscribe(encode => {
      this.setFormValue({
        decode:(<Encode>encode).encode,
        encode: '',
        password: this.form.value.password,
      })
    });
  };

  onDecode() {
    const decode: Code = {
      password: this.form.value.password,
      message: this.form.value.decode,
    };
    this.codeService.getDecode(decode).subscribe(encode => {
      this.setFormValue({
        decode:this.form.value.encode,
        encode: (<Decode>encode).decode,
        password: this.form.value.password,
      })
    });
  }


  setFormValue(value: {[key:string]: any}){
    setTimeout(() => {
      this.form.setValue(value);
    })
  }
}
