import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Code } from '../models/code.model';


@Injectable({
  providedIn: 'root'
})
export class CodeService {

  constructor(private http: HttpClient) { }


  getEncode(code: Code){
    console.log(code);
    return this.http.post('http://localhost:8000/encode', code);
  }

  getDecode(code: Code){
    return this.http.post('http://localhost:8000/decode', code);
  }
}
