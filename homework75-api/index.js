const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const express = require('express');
const app = express();
const port = 8000;


app.use(express.json());
app.use(cors({origin:'http://localhost:4200'}));

app.post('/encode', (req, res) => {
  console.log(req.body);
  const encode = {
    encode: Vigenere.Cipher(req.body.password).crypt(req.body.message)
  }
  return res.send(encode);
});

app.post('/decode', (req, res) => {
  const decode = {
    decode: Vigenere.Decipher(req.body.password).crypt(req.body.message)
  }
  return res.send(decode);
});


app.listen(port, () => {
  console.log('We are live on ' + port);
});


